#!/usr/bin/env node
exports.init = async () => {
  const shell = require('shelljs')
  const fuzzy = require('fuzzy')
  const inquirer = require('inquirer')
  inquirer.registerPrompt(
    'autocomplete',
    require('inquirer-autocomplete-prompt')
  )
  const fs = require('fs')
  const homedir = require('os').homedir()

  const { prompt, input, getRestData } = require('./functions')

  shell.rm('-rf', homedir + '.standard-jira-ticketrc.json')
  console.log('Configuring tool')
  const osUsername = require('os').userInfo().username
  const endpoint = await input('JIRA Domain (no https://):', {
    filter: (input) => `https://${input}`,
  })

  const username = await input('JIRA Username:', { default: osUsername })
  const passwordResponse = await inquirer.prompt([
    {
      name: 'password',
      type: 'password',
      message: 'Jira Password',
    },
  ])
  const projects = getRestData('rest/api/2/project', passwordResponse.password, endpoint, username)
  const projectKeys = projects.map((project) => project.key)
  let selectedProjects = []
  let addProject

  const selectProject = async () => {
    const select = (message, options) =>
      inquirer.prompt([
        {
          type: 'autocomplete',
          name: 'result',
          message,
          pageSize: 10,
          source: (answersSoFar, input) =>
            new Promise((resolve, reject) => {
              if (input) {
                let filteredOptions = fuzzy.filter(
                  input,
                  options.filter((option) => !selectedProjects.includes(option))
                )
                if (filteredOptions) {
                  resolve(filteredOptions.map((el) => el.string))
                  return
                }
              }
              resolve(options)
            }).catch((e) => {}),
        },
      ])

    const { result } = await select(
      'Choose project to configure for autocomplete:',
      projectKeys
    )
    selectedProjects = [...selectedProjects, result]
  }

  await selectProject()

  inquirer.registerPrompt(
    'autocomplete',
    require('inquirer-autocomplete-prompt')
  )

  addProject = await prompt('Would you add another project?', 'Yes')

  while (addProject) {
    await selectProject()
    inquirer.registerPrompt(
      'autocomplete',
      require('inquirer-autocomplete-prompt')
    )

    addProject = await prompt('Would you add another project?', 'Yes')
  }
  const enableIncidents = await prompt("Enable Incidents Feature?", "Yes")
  const enableScopes = await prompt("Enable Scopes Feature?", "Yes")
  const enableSourcing = await prompt("Enable Sourcing Feature?", "Yes")
  const enableSprints = await prompt("Work with Sprints?", "Yes")
  const enableEpics = await prompt("Work with Epics?", "Yes")

  shell.touch(homedir + '/.standard-jira-ticketrc.json')
  let config = JSON.stringify({
    username,
    endpoint,
    projects: selectedProjects,
    enableIncidents,
    enableSprints,
    enableScopes,
    enableEpics,
    enableSourcing,
  })
  fs.writeFileSync(homedir + '/.standard-jira-ticketrc.json', config, function (
    err
  ) {
    if (err) return console.log(err)
  })
  console.log('Configuration finished.')
}
