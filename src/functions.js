const inquirer = require('inquirer')
const slugify = require('slugify')
const shell = require('shelljs')
const { cosmiconfigSync } = require('cosmiconfig')
const explorerSync = cosmiconfigSync('standard-jira-ticket')
exports.prompt = async (message, initial = undefined) => {
  const prompt = await inquirer.prompt([
    {
      name: 'confirm',
      message,
      default: initial,
    },
  ])
  return (
    prompt.confirm.toLowerCase() === 'yes' ||
    prompt.confirm.toLowerCase() === 'y'
  )
}

exports.jiraTicketFormatter = (input) => {
  if (input.indexOf('-') === -1) {
    let suffix = ''
    if (input.slice(-1) === '-' || input.slice(-1) === ' ') {
      suffix = '-'
    }

    return slugify(input).toUpperCase() + suffix
  }

  const pieces = slugify(input).split('-')
  return pieces[0].toUpperCase() + '-' + pieces[1]
}

exports.input = async (message, options) => {
  const mergedOptions = {
    default: '',
    ...options,
  }
  const response = await inquirer.prompt([
    {
      name: 'name',
      message,
      default: mergedOptions.default,
      filter: mergedOptions.filter,
      validate: mergedOptions.validate,
      transformer: mergedOptions.transformer,
    },
  ])
  return response.name
}

exports.getRestData = (path, password, endpoint = null, user = null) => {
  if (!endpoint || !user) {
    let configResults = explorerSync.search()
    if (!configResults) {
      throw new Error('Config not set')
    }
    let config = configResults.config

    if (!user) {
      user = config.username
    }
    if (!endpoint) {
      endpoint = config.endpoint
    }
  }
  if (!user) {
    user = config.username
  }
  return JSON.parse(
    shell.exec(`curl -s -u ${user}:${password} -X GET -H "Content-Type: application/json" ${endpoint}/${path}`, { silent: true }).stdout.replace('\n', '').trim()
  )
}

exports.searchForTicket = ({ project, query }, password) => {
  let configResults = explorerSync.search()
  if (!configResults) {
    throw new Error('Endpoint not set')
  }
  let config = configResults.config
  return JSON.parse(
    shell
      .exec(
        `curl -N -u "${config.username}:${password}" -X POST -H "Content-Type: application/json" --data '{"jql":"project = ${project} AND text ~ \\"${query}*\\" ORDER BY updated DESC","startAt":0,"maxResults":10,"fields":["key","issuetype","summary","assignee","status","created","updated"]}' "${config.endpoint}/rest/api/2/search" `,
        { silent: true }
      )
      .stdout.replace('\n', '')
      .trim()
  ).issues
}
