# JIRA Ticket Command Line Interface

This helper creates a JIRA issue using the API.

## Setup

```
npm install -g standard-jira-issue
ticket --init
```

## Usage

- Run the `standard-ticket`, `ticket`, `standard-issue` or `issue` command.
- Answer the interactive questions
- See `ticket --help` for more details

# Features

## General

- Interactive questions to get the needed data
- Fuzzy Search for Tickets, Projects etc.
- Configuration generator
- Can be scripted. See `ticket --help`
- Can Open created Ticket im Browser

## Sprints

- Assign Tickets to active or future sprints
- Fuzzy Search for future Sprints from the Terminal

## Epics

- Assign Tickets to Epics
- Fuzzy Search for Epics from the Terminal

## Sourcing

- Set a Ticket as a starting point.
  - The new Ticket will be linked to the sourced Ticket
  - The new Ticket can have the same title as the sourced Ticket (auto-fill - can be changed)

## Incidents

- Task Type 'Bug' automatically
  \*\* No Time Estimate
- `Incident -` Prefix
- Suggestion to add to active sprint

## Scopes

- Prefix in title as suggestion
  - If incidents, it comes after the Incident prefix
  - automatically filled if the command is executed in a git repo
