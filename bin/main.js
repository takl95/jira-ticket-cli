#!/usr/bin/env node

;(async () => {
  const shell = require('shelljs')
  const fuzzy = require('fuzzy')
  const chalk = require('chalk')
  const program = require('commander')
  const inquirer = require('inquirer')

  inquirer.registerPrompt(
    'autocomplete',
    require('inquirer-autocomplete-prompt')
  )
  const ora = require('ora')
  let password
  let prio

  program
    .option('--init', 'reconfigure the tool')
    .option('-j, --project <project>', 'JIRA Project of the Ticket')
    .option('-p, --pass <password>', 'You JIRA password')
    .option('-y, --yes', 'Skip manual check before saving')
    .option('-t, --title <title>', 'Title (Summary) of the Ticket')
    .option('-e, --estimate <estimate>', 'Time estimate for the Ticket')
    .option('-n, --notty', 'Disable all prompts')
    .option(
      '-s, --scope <scope>',
      'Scope of the Ticket. (suggestions as part of the title if no title is specified).  Requires the enableScope option'
    )
    .option('-d, --desc <desc>', 'Description of the Ticket')
    .option('-r, --related <related>', 'Related Ticket')
    .option('--prio <prio>', 'Priority of the issue (text)')
    .option(
      '-a, --assignee <assignee>',
      'Assigne of the Ticket. Use "@me" to assign to the configured username'
    )
    .option(
      '-i, --incident',
      'Mark as incident. Requires the enableIncident option'
    )
    .option('--no-epic', 'Ignore the Epic option for this call')
    .option(
      '-c, --active-sprint',
      'Add to active Sprint. Requires the enableSprints option'
    )

  program.parse(process.argv)
  const { init } = require('../src/setup')
  const {
    getRestData,
    prompt,
    input,
    jiraTicketFormatter,
    searchForTicket,
  } = require('../src/functions')

  if (program.init) await init()
  const { cosmiconfigSync } = require('cosmiconfig')
  const explorerSync = cosmiconfigSync('standard-jira-ticket')
  let configResults = explorerSync.search()
  if (!configResults) {
    await init()
  }
  let issuelinks = []
  let config = configResults.config
  if (program.pass) {
    password = program.pass
  }
  if (!password) {
    const passwordResponse = await inquirer.prompt([
      {
        name: 'password',
        type: 'password',
        message: 'Jira Password',
      },
    ])
    password = passwordResponse.password
  }
  try {
    let login = await getRestData('rest/api/2/priority', password)
  } catch (e) {
    console.log('Login failed.')
    return
  }
  inquirer.registerPrompt(
    'autocomplete',
    require('inquirer-autocomplete-prompt')
  )
  let sourcingTicket
  if (program.related) {
    if(!program.notty )
      console.log(`Linking ${program.relates}`)
    const linkTypeSpinner = ora('Loading possible issue types').start();
    let linkTypes = await getRestData('rest/api/2/issueLinkType', password)
      .issueLinkTypes
    let linkTypeNames = linkTypes.map((type) => type.name)
    linkTypeSpinner.stop()
    let linkType = linkTypes[linkTypes.length - 1]
    if (!program.notty) {
      let linkTypeSelect = await inquirer.prompt([
        {
          type: 'autocomplete',
          name: 'result',
          message: 'Link Type:',
          pageSize: 10,
          source: (answersSoFar, input) =>
            new Promise((resolve, reject) => {
              if (input) {
                let filteredOptions = fuzzy.filter(input, linkTypeNames)
                if (filteredOptions) {
                  resolve(filteredOptions.map((el) => el.string))
                  return
                }
              }
              resolve(linkTypeNames)
            }).catch((e) => {
            }),
        },
      ]);
      linkType = linkTypes.find((link) => link.name === linkTypeSelect.result)
    }

    issuelinks = [
      ...issuelinks,
      {
        add: {
          type: {
            name: linkType.name,
            inward: linkType.inward,
            outward: linkType.outward,
          },
          outwardIssue: {
            key: program.related,
          },
        },
      },
    ]
  }

  if (!issuelinks.length && config.enableSourcing && !program.notty) {
    if (
      await prompt(
        'Would you like to start from an existing Ticket? (Creating Linked Issue)',
        'No'
      )
    ) {
      const sourcingQueryTypeResponse = await inquirer.prompt([
        {
          type: 'list',
          name: 'response',
          message: 'How would you like to specify the Ticket to start from?',
          choices: ['Search for a Ticket', 'Input Ticket Key'],
        },
      ])
      let sourcingKey
      switch (sourcingQueryTypeResponse.response) {
        case 'Input Ticket Key':
          let sourcingValid = true
          do {
            if (!sourcingValid) {
              console.log('Ticket invalid')
            }

            sourcingKey = await input('(Sourcing)Ticket Key:', {
              filter: jiraTicketFormatter,
              transformer: jiraTicketFormatter,
            })
            try {
              const sourcingValidationSpinner = ora('Validating Ticket').start()
              const results = await getRestData(
                'rest/api/2/issue/' + sourcingKey,
                password
              )
              if (results.errorMessages && results.errorMessages.length > 0) {
                sourcingValid = false
                sourcingValidationSpinner.stop()
              } else {
                sourcingValidationSpinner.stop()
                sourcingValid = await prompt(
                  `Are you sure, you want to sourcing '${results.fields.summary}'?`,
                  'Yes'
                )
                sourcingTicket = {
                  key: sourcingKey,
                  summary: results.fields.summary,
                }
              }
            } catch (e) {
              sourcingValid = false
            }
          } while (!sourcingValid)
          break
        case 'Search for a Ticket':
          let searchProject = await inquirer.prompt([
            {
              type: 'autocomplete',
              name: 'result',
              message: 'In which JIRA Project would you like to search?',
              pageSize: 10,
              source: (answersSoFar, input) =>
                new Promise((resolve, reject) => {
                  if (input) {
                    let filteredOptions = fuzzy.filter(input, config.projects)
                    if (filteredOptions) {
                      resolve(filteredOptions.map((el) => el.string))
                      return
                    }
                  }
                  resolve(config.projects)
                }).catch((e) => {}),
            },
          ])
          searchProject = searchProject.result
          let searchQuery
          let searchResults
          let searchResultOptions
          do {
            searchQuery = await input('Type in what to search for:')
            searchResults = await searchForTicket(
              { project: searchProject, query: searchQuery },
              password
            )
            searchResultOptions = searchResults.map(
              (item) => item.key + '_>_' + item.fields.summary
            )
            if (!searchResults.length) {
              console.log("Couldn't find anything. Try something else")
            }
          } while (!searchResults.length)
          let selectSearchResult = await inquirer.prompt([
            {
              type: 'autocomplete',
              name: 'result',
              message: 'Choose a Ticket to sourcing:',
              pageSize: 10,
              source: (answersSoFar, input) =>
                new Promise((resolve, reject) => {
                  if (input) {
                    let filteredOptions = fuzzy.filter(
                      input,
                      searchResultOptions
                    )
                    if (filteredOptions) {
                      resolve(filteredOptions.map((el) => el.string))
                      return
                    }
                  }
                  resolve(searchResultOptions)
                }).catch((e) => {}),
            },
          ])
          sourcingKey = selectSearchResult.result.split('_>_')[0]
          sourcingTicket = {
            key: selectSearchResult.result.split('_>_')[0],
            summary: selectSearchResult.result.split('_>_')[1],
          }
          break
      }

      if (!sourcingKey || !sourcingTicket) {
        console.log('Sourcing Ticket Invalid')
        return
      }

      const linkTypeSpinner = ora('Loading possible issue types').start()
      let linkTypes = await getRestData('rest/api/2/issueLinkType', password)
        .issueLinkTypes
      let linkTypeNames = linkTypes.map((type) => type.name)
      linkTypeSpinner.stop()
      let linkTypeSelect = await inquirer.prompt([
        {
          type: 'autocomplete',
          name: 'result',
          message: 'Link Type:',
          pageSize: 10,
          source: (answersSoFar, input) =>
            new Promise((resolve, reject) => {
              if (input) {
                let filteredOptions = fuzzy.filter(input, linkTypeNames)
                if (filteredOptions) {
                  resolve(filteredOptions.map((el) => el.string))
                  return
                }
              }
              resolve(linkTypeNames)
            }).catch((e) => {}),
        },
      ])
      let linkType = linkTypes.find(
        (link) => link.name === linkTypeSelect.result
      )
      issuelinks = [
        ...issuelinks,
        {
          add: {
            type: {
              name: linkType.name,
              inward: linkType.inward,
              outward: linkType.outward,
            },
            outwardIssue: {
              key: sourcingKey,
            },
          },
        },
      ]
    }
  }
  let project = ''
  if (program.project) {
    project = program.project.toUpperCase()
  }
  if (project && !config.projects.includes(project)) {
    console.log(`Project ${project} is not specified in the config!`)
    project = ''
  }
  if (!project) {
    if (program.notty) {
      throw new Error("No Project specified")
    }
    let projectSelect = await inquirer.prompt([
      {
        type: 'autocomplete',
        name: 'result',
        message: 'JIRA Project:',
        pageSize: 10,
        source: (answersSoFar, input) =>
          new Promise((resolve, reject) => {
            if (input) {
              let filteredOptions = fuzzy.filter(input, config.projects)
              if (filteredOptions) {
                resolve(filteredOptions.map((el) => el.string))
                return
              }
            }
            resolve(config.projects)
          }).catch((e) => {
          }),
      },
    ]);
    project = projectSelect.result
  }
  let assignee = null
  if (program.assignee) {
    if (program.assignee === '@me') {
      assignee = config.username
    } else {
      assignee = program.assignee
    }
  }
  let isIncident = false

  if (config.enableIncidents) {
    if (program.incident) {
      isIncident = true
    } else if (!program.notty) {
      isIncident = await prompt('Is this an Incident?', 'No')
    }
  }

  let type = 'Task'
  if (isIncident) {
    type = 'Bug'
  }
  if (!isIncident && !program.notty) {
    const typeSpinner = ora('Loading possible issue types').start()
    let issueTypes = getRestData(
      '/rest/api/2/project/' + project,
      password
    ).issueTypes.map((type) => type.name)
    typeSpinner.stop()
    let typeSelect = await inquirer.prompt([
      {
        type: 'autocomplete',
        name: 'result',
        message: 'Issue Type:',
        pageSize: 10,
        source: (answersSoFar, input) =>
          new Promise((resolve, reject) => {
            if (input) {
              let filteredOptions = fuzzy.filter(input, issueTypes)
              if (filteredOptions) {
                resolve(filteredOptions.map((el) => el.string))
                return
              }
            }
            resolve(issueTypes)
          }).catch((e) => {}),
      },
    ])
    type = typeSelect.result
  }
  if (program.prio) {
    prio = getRestData('rest/api/2/priority', password).find(
      (priority) => priority.name === program.prio
    ).id
    if (!prio) {
      console.log(`Error: Priority '${program.prio}' is invalid`)
      return
    }
  } else {
    if (await prompt('Set Issue Priority?', 'No')) {
      const prioSpinner = ora('Loading possible issue prios').start()
      let issuePrios = getRestData('rest/api/2/priority', password)
      const issuePrioOptions = issuePrios.map((prio) => prio.name)
      prioSpinner.stop()

      let prioSelect = await inquirer.prompt([
        {
          type: 'autocomplete',
          name: 'result',
          message: 'Issue Priority:',
          pageSize: 9,
          source: (answersSoFar, input) =>
            new Promise((resolve, reject) => {
              if (input) {
                let filteredOptions = fuzzy.filter(input, issuePrioOptions)
                if (filteredOptions) {
                  resolve(filteredOptions.map((el) => el.string))
                  return
                }
              }
              resolve(issuePrioOptions)
            }).catch((e) => {}),
        },
      ])
      prio = issuePrios.find((prio) => prio.name === prioSelect.result).id
    }
  }
  let scope = ''
  if (config.enableScopes && !program.title) {
    if (program.scope) {
      scope = program.scope
    }
    if (!scope) {
      let defaultScope = ''

      if (shell.which('git')) {
        try {
          defaultScope = shell
            .exec('git config --get remote.origin.url', { silent: true })
            .stdout.replace('.git', '')
            .split('/')[1]
            .replace('\n', '')
        } catch (e) {
          defaultScope = ''
        }
      }

      scope = await input('GIT Project / Scope', { default: defaultScope })
    }
  }

  let title
  if (program.title) {
    title = program.title
  }
  if (!title) {
    if (isIncident) {
      if (scope) {
        title = `${await input('Ticket title', {
          transformer: (input) => `Incident - ${scope} - ${input}`,
          default:
            config.enableSourcing && sourcingTicket && sourcingTicket.summary
              ? sourcingTicket.summary
              : '',
        })}`
        title = `Incident - ${scope} - ${title}`
      } else {
        title = `${await input('Ticket title', {
          transformer: (input) => `Incident - ${input}`,
          default:
            config.enableSourcing && sourcingTicket && sourcingTicket.summary
              ? sourcingTicket.summary
              : '',
        })}`
        title = `Incident - ${title}`
      }
    } else {
      if (scope) {
        title = `${await input('Ticket title', {
          transformer: (input) => `${scope} - ${input}`,
          default:
            config.enableSourcing && sourcingTicket && sourcingTicket.summary
              ? sourcingTicket.summary
              : '',
        })}`
        title = `${scope} - ${title}`
      } else {
        title = `${await input('Ticket title', {
          default:
            config.enableSourcing && sourcingTicket && sourcingTicket.summary
              ? sourcingTicket.summary
              : '',
        })}`
      }
    }
  }
  let description
  if (program.desc) {
    description = program.desc
  } else {
    description = await input('Ticket description')
  }
  let estimate = ''
  if (!isIncident) {
    if (program.estimate) {
      estimate = program.estimate
    }
    if (!estimate) {
      estimate = await input('Time Estimate', {
        validate: (input) =>
          input.match(/^(?:(\d+)h\s*)?(?:(\d+)m\s*)?$/)
            ? true
            : "Incorrent estimate. (Must have the format '42h 42m'",
      })
    }
  }

  let sprint = ''
  if (config.enableSprints) {
    if (program.activeSprint) {
      sprint = await getRestData(
        '/rest/agile/1.0/board/66/sprint?state=active',
        password
      ).values[0].id
    }
    if (!sprint) {
      if (isIncident) {
        if (await prompt('Incident Ticket - Add to active Sprint?', 'Yes')) {
          sprint = await getRestData(
            '/rest/agile/1.0/board/66/sprint?state=active',
            password
          ).values[0].id
        }
      } else {
        if (
          await prompt(
            'Add Ticket to Sprint? (If not it will be in the Backlog)',
            'Yes'
          )
        ) {
          if (await prompt('Add to active Sprint?', 'No')) {
            sprint = await getRestData(
              '/rest/agile/1.0/board/66/sprint?state=active',
              password
            ).values[0].id
          } else {
            const futureSprints = await getRestData(
              '/rest/agile/1.0/board/66/sprint?state=future',
              password
            ).values
            const futureSprintOptions = futureSprints.map((epic) => epic.name)
            let sprintSelect = await inquirer.prompt([
              {
                type: 'autocomplete',
                name: 'result',
                message: 'Future Sprint:',
                pageSize: 10,
                source: (answersSoFar, input) =>
                  new Promise((resolve, reject) => {
                    if (input) {
                      let filteredOptions = fuzzy.filter(
                        input,
                        futureSprintOptions
                      )
                      if (filteredOptions) {
                        resolve(filteredOptions.map((el) => el.string))
                        return
                      }
                    }
                    resolve(futureSprintOptions)
                  }).catch((e) => {}),
              },
            ])
            sprint = futureSprints.find(
              (item) => item.name === sprintSelect.result
            ).id
          }
        }
      }
    }
  }

  let epic = ''
  if (config.enableEpics && program.epic) {
    if (await prompt('Add Ticket to Epic?', 'No')) {
      const epics = await getRestData(
        'rest/agile/1.0/board/66/epic?done=false',
        password
      ).values
      const epicOptions = epics.map((epic) => epic.name)
      let epicSelect = await inquirer.prompt([
        {
          type: 'autocomplete',
          name: 'result',
          message: 'Epic:',
          pageSize: 10,
          source: (answersSoFar, input) =>
            new Promise((resolve, reject) => {
              if (input) {
                let filteredOptions = fuzzy.filter(input, epicOptions)
                if (filteredOptions) {
                  resolve(filteredOptions.map((el) => el.string))
                  return
                }
              }
              resolve(epicOptions)
            }).catch((e) => {}),
        },
      ])
      epic = epics.find((item) => item.name === epicSelect.result).id
    }
  }

  if (
    !assignee &&
    (await prompt('Would you like to assign the Ticket?', 'Yes'))
  ) {
    if (await prompt('Assign to yourself? (' + config.username + ')', 'Yes')) {
      assignee = config.username
    } else {
      const assigneeSpinner = ora('Loading possible assignees').start()
      let possibleAssignees = await getRestData(
        'rest/api/2/user/assignable/search?project=' + project,
        password
      ).map((person) => person.key)
      assigneeSpinner.stop()
      let assigneeSelect = await inquirer.prompt([
        {
          type: 'autocomplete',
          name: 'result',
          message: 'Assign:',
          pageSize: 10,
          source: (answersSoFar, input) =>
            new Promise((resolve, reject) => {
              if (input) {
                let filteredOptions = fuzzy.filter(input, possibleAssignees)
                if (filteredOptions) {
                  resolve(filteredOptions.map((el) => el.string))
                  return
                }
              }
              resolve(possibleAssignees)
            }).catch((e) => {}),
        },
      ])
      assignee = assigneeSelect.result
    }
  }
  if (
    !issuelinks.length &&
    (await prompt('Would you like to link a related Ticket?', 'No'))
  ) {
    const ticketQueryTypeResponse = await inquirer.prompt([
      {
        type: 'list',
        name: 'response',
        message: 'How would you like to specify the related Ticket?',
        choices: ['Search for a Ticket', 'Input Ticket Key'],
      },
    ])
    let ticketKey
    switch (ticketQueryTypeResponse.response) {
      case 'Input Ticket Key':
        let ticketValid = true
        do {
          if (!ticketValid) {
            console.log('Ticket invalid')
          }

          ticketKey = await input('Ticket Key:', {
            filter: jiraTicketFormatter,
            transformer: jiraTicketFormatter,
          })
          try {
            const ticketValidationSpinner = ora('Validating Ticket').start()
            const results = await getRestData(
              'rest/api/2/issue/' + ticketKey,
              password
            )
            if (results.errorMessages && results.errorMessages.length > 0) {
              ticketValid = false
              ticketValidationSpinner.stop()
            } else {
              ticketValidationSpinner.stop()
              ticketValid = await prompt(
                `Are you sure, you want to link '${results.fields.summary}'?`,
                'Yes'
              )
            }
          } catch (e) {
            ticketValid = false
          }
        } while (!ticketValid)
        break
      case 'Search for a Ticket':
        let searchProject = await inquirer.prompt([
          {
            type: 'autocomplete',
            name: 'result',
            message: 'In which JIRA Project would you like to search?',
            pageSize: 10,
            source: (answersSoFar, input) =>
              new Promise((resolve, reject) => {
                if (input) {
                  let filteredOptions = fuzzy.filter(input, config.projects)
                  if (filteredOptions) {
                    resolve(filteredOptions.map((el) => el.string))
                    return
                  }
                }
                resolve(config.projects)
              }).catch((e) => {}),
          },
        ])
        searchProject = searchProject.result
        let searchQuery
        let searchResults
        let searchResultOptions

        do {
          searchQuery = await input('Type in what to search for:')
          searchResults = await searchForTicket(
            { project: searchProject, query: searchQuery },
            password
          )
          searchResultOptions = searchResults.map(
            (item) => item.key + '_>_' + item.fields.summary
          )
          if (!searchResults.length) {
            console.log(`No Result for '${searchQuery}' Try something else`)
          }
        } while (!searchResults.length)
        let selectSearchResult = await inquirer.prompt([
          {
            type: 'autocomplete',
            name: 'result',
            message: 'Choose a Ticket to link:',
            pageSize: 10,
            source: (answersSoFar, input) =>
              new Promise((resolve, reject) => {
                if (input) {
                  let filteredOptions = fuzzy.filter(input, searchResultOptions)
                  if (filteredOptions) {
                    resolve(filteredOptions.map((el) => el.string))
                    return
                  }
                }
                resolve(searchResultOptions)
              }).catch((e) => {}),
          },
        ])
        ticketKey = selectSearchResult.result.split('_>_')[0]
        break
    }

    if (!ticketKey) {
      console.log('Ticket Invalid')
      return
    }

    const linkTypeSpinner = ora('Loading possible issue types').start()
    let linkTypes = await getRestData('rest/api/2/issueLinkType', password)
      .issueLinkTypes
    let linkTypeNames = linkTypes.map((type) => type.name)
    linkTypeSpinner.stop()
    let linkTypeSelect = await inquirer.prompt([
      {
        type: 'autocomplete',
        name: 'result',
        message: 'Link Type:',
        pageSize: 10,
        source: (answersSoFar, input) =>
          new Promise((resolve, reject) => {
            if (input) {
              let filteredOptions = fuzzy.filter(input, linkTypeNames)
              if (filteredOptions) {
                resolve(filteredOptions.map((el) => el.string))
                return
              }
            }
            resolve(linkTypeNames)
          }).catch((e) => {}),
      },
    ])
    let linkType = linkTypes.find((link) => link.name === linkTypeSelect.result)
    issuelinks = [
      ...issuelinks,
      {
        add: {
          type: {
            name: linkType.name,
            inward: linkType.inward,
            outward: linkType.outward,
          },
          outwardIssue: {
            key: ticketKey,
          },
        },
      },
    ]
    //
  }
  let data
  if (isIncident) {
    data = {
      fields: {
        project: {
          key: project,
        },
        summary: title,
        description,
        assignee: {
          name: assignee,
        },
        issuetype: {
          name: type,
        },
      },
      update: {
        issuelinks: issuelinks,
      },
    }
  } else {
    data = {
      fields: {
        project: {
          key: project,
        },
        summary: title,
        description,
        assignee: {
          name: assignee,
        },
        timetracking: {
          originalEstimate: estimate,
          remainingEstimate: estimate,
        },
        issuetype: {
          name: type,
        },
      },
      update: {
        issuelinks: issuelinks,
      },
    }
  }
  if (prio) {
    data.fields.priority = { id: prio }
  }
  console.clear()
  const util = require('util')
  console.log(' ')
  console.log(' ')
  console.log(' ')
  console.log(util.inspect(data, { colors: true, depth: null }))
  console.log(' ')
  console.log(' ')
  console.log(' ')
  let confirmation
  if (!program.yes) {
    confirmation = await input('Create Ticket with the data above?', {
      validate: (input) =>
        ['y', 'n', 'yes', 'no'].includes(input.toLowerCase())
          ? true
          : 'Answer with y/n',
    })
  }
  if (program.yes || confirmation === 'y' || confirmation === 'yes') {
    let cmd = `curl -s -u ${
      config.username
    }:${password} -X POST --data '${JSON.stringify(
      data
    )}' -H "Content-Type: application/json" https://ats.anexia-it.com/rest/api/2/issue/ `
    let result = shell
      .exec(cmd, { silent: true })
      .stdout.replace('\n', '')
      .trim()

    if (!JSON.parse(result)) {
      console.log({ output: result })
      console.log('Something went wrong with parsing the result')
      return
    }

    result = JSON.parse(result)
    if (!result.key) {
      console.log({ output: result })
      console.log('Something went wrong.')
      return
    }

    if (epic) {
      let epicResults = shell
        .exec(
          `curl -s -u ${config.username}:${password} -X PUT -H "Content-Type: application/json" --data '{"ignoreEpics":true,"issueKeys":["${result.key}"]}' "${config.endpoint}/rest/greenhopper/1.0/epics/${epic}/add"`,
          { silent: true }
        )
        .stdout.replace('\n', '')
        .trim()
    }

    if (sprint) {
      let cmd = `curl -s -u ${config.username}:${password} -X POST -H "Content-Type: application/json" --data '{"issues":["${result.key}"]}' "${config.endpoint}/rest/agile/1.0/sprint/${sprint}/issue"`
      let sprintResults = shell
        .exec(cmd, { silent: true })
        .stdout.replace('\n', '')
        .trim()
    }

    if (
      await prompt(`Ticket "${result.key}" created. Open in Browser?`, 'Yes')
    ) {
      const open = require('open')
      open(`${config.endpoint}/browse/${result.key}`)
      console.log('Opened:')
      console.log(`${config.endpoint}/browse/${result.key}`)
    }
  }
})()
